import Content from "@components/content"
import Banner from "@components/banner"
import Clients from "@components/clients"
import Business from "@components/business"
import Solutions from "@components/solutions"
import Testemonials from "@components/testemonials"
import News from "@components/news"
import Suscription from "@components/suscription"
import Space from "@components/space"

import Info from "@data/home"

const Index = () => {
    
    return (
        <>
           <Content>
                <Banner {...Info.banner}/>
                <Space px={80}/>
                <Clients {...Info.clients}/>
                <Business {...Info.business}/>
                <Solutions {...Info.solutions}/>
                <Testemonials {...Info.testemonials}/>
                <News {...Info.news}/>
                <Suscription {...Info.suscription}/>
           </Content>
        </>
    )
}
export default Index