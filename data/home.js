export default {
    banner: {
        title: "Marketing is The Key of Business Sucess.",
        text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been ",

        link1: {
            text: "Get Started",
            link: "/",
        },
        link2: {
            text: "Watch Our Video",
            link: "/",
        },

        bg: "senor.png",

    },
    clients: {
        textTitle:"See how much our client love.",
        imgBarra:"barra.png",
        title: "Take Your Online marketing to the next level.",
        img: "clients.png",
        clients: [
            {
                title: "Direct Scheduling",
                text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                img: "calendario.svg"
            },
            {
                title: "Reminders",
                text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                img: "campana.svg"
            },
            {
                title: "Shop Grid",
                text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                img: "tienda.svg"
            },
        ]
    },
    business: {

        title: "Want you to Boost your Business?",
        text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        card:[
            {
                img: "business-1.svg",
                title:"Scheduling",
                text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry",
                link:"/",
                textLink:"Read more",
            },
            {
                img: "business-2.svg",
                title:"Increase conversion",
                text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry",
                link:"/",
                textLink:"Read more",
            },
            {
                img: "business-3.svg",
                title:"Increase analytics",
                text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry",
                link:"/",
                textLink:"Read more",
            },
            
        ]
    },
    solutions: {

        bg:"solutions.png",

        title: "Unique Solutions for Your Business",
        text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.",
        infoNumber:[
            {
                number: "50+",
                color:"color-pink-2",
                text:"Total Client"
            },
            {
                number:"100+",
                color:"color-purple-2",
                text: "Project Done",
            },
        ]
    },
    testemonials: {

        title: "TESTEMONIALS",
        text: "What The People Thinks About Us",
        card:[
            {
                img: "foto-testemonials.png",
                title:"Increase analytics",
                subTitle:"One Year With Us",
                text:"Lorem ipsum dolor sit amet, consec adipis. Cursus ultricies sit sit",
            },
            
            {
                img: "foto-testemonials.png",
                title:"Increase analytics",
                subTitle:"One Year With Us",
                text:"Lorem ipsum dolor sit amet, consec adipis. Cursus ultricies sit sit",
            },
            
            {
                img: "foto-testemonials.png",
                title:"Increase analytics",
                subTitle:"One Year With Us",
                text:"Lorem ipsum dolor sit amet, consec adipis. Cursus ultricies sit sit",
            },
            
        ]
    },
    news : {

        title: "Latest News",
        text: "There are many variations of passages of lorem lpsum available, but the majority have suffered alteraction",
        card:[
            {
                img: "news-1.png",
                textImg:"Jack Wilson",
                imgA:"logo-tipo.svg",
                textPart:"06 March 2022",
                imgP:"logo-calendario.svg",
                text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry",
            },
            {
                img: "news-2.png",
                textImg:"Jack Wilson",
                imgA:"logo-tipo.svg",
                textPart:"06 March 2022",
                imgP:"logo-calendario.svg",
                text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry",
            },
            {
                img: "news-3.png",
                textImg:"Jack Wilson",
                imgA:"logo-tipo.svg",
                textPart:"06 March 2022",
                imgP:"logo-calendario.svg",
                text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry",
            },
            
        ]
    },
    suscription: {
        subTitle:"Join Our Comunity",
        title: "Subscribe To Our Newsletter",
        text:"There are many variations of passages of lorem lpsum available, but the majority have suffered alteraction",
    },

}