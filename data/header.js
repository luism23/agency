export default {
    linkp:{
        title: "Agency",
        link: "/"
    },
    links: [
        {
            title: "Home",
            link: "/"
        },
        {
            title: "About us",
            link: "/about-us"
        },
        {
            title: "Service",
            link: "/service"
        },
        {
            title: "Pricing",
            link: "/pricing"
        }
    ],
    buttom: {
        title: "Sign Up",
        link: "/"
    }
}