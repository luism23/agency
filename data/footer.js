export default {
    title:"Agency",
    text:"There are many variations of passages of lorem lpsum available, but the majority have suffered alteraction",
    rs: [
        {
            link: "/",
            img: "instagram.svg"
        },
        {
            link: "/",
            img: "facebook.svg"
        },
        {
            link: "/",
            img: "twiter.svg"
        },
    ],
    cols: [
        {
            title: "Product",
            links: [
                {
                    link: "/",
                    text: "Employee database"
                },
                {
                    link: "/",
                    text: "Payroll"
                },
                {
                    link: "/",
                    text: "Absences"
                },
                {
                    link: "/",
                    text: "Time tracking"
                },
            ]
        },
        {
            title: "Information",
            links: [
                {
                    link: "/",
                    text: "FAQs"
                },
                {
                    link: "/",
                    text: "Blog"
                },
                {
                    link: "/",
                    text: "Support"
                },
                {
                    link: "/",
                    text: "Payroll"
                }
            ]
        },
        {
            title: "Company",
            links: [
                {
                    link: "/",
                    text: "About"
                },
                {
                    link: "/",
                    text: "Careers"
                },
                {
                    link: "/",
                    text: "Contact"
                }
            ]
        }

    ],
    textBottom:"© 2021 Real State - All Rights Reserved.",
}