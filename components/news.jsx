const Cards = ({ title = "", text = "", textLink = "", link = "", img = "", imgP = "", imgA = "", textImg = "", textPart = "" }) => {
    return (
        <>
            <div className="news-info ">
                <div className="news-info-content-img">
                    <img className="news-info-img" src={"/img/" + img} alt="" />

                </div>
                <div className="news-info-content">
                    <div className="news-info-content-description">
                        <div className="news-info-content-description-1">
                            <h5 className="news-info-content-description-1-text color-gray font-Jost font-18">
                                <img className="news-info-content-description-1-text-img" src={"/img/svg/" + imgA} alt="" />
                                {textPart}
                            </h5>
                        </div>
                        <div className="news-info-content-description-2">
                            <h5 className="news-info-content-description-2-text color-gray font-Jost font-18">
                                <img className="news-info-content-description-2-text-img" src={"/img/svg/" + imgP} alt="" />
                                {textImg}
                            </h5>
                        </div>
                    </div>
                    <p className="news-info-content-text color-gray-lite font-24 font-Jost font-w-600">
                        {text}
                    </p>
                </div>
            </div>
        </>

    )
}

const Index = ({ title, text, card = [] }) => {
    return (
        <>
            <div className="news container">
                <div className="news-content">
                    <h1 className="news-content-title color-gray-lite font-48 font-w-700 font-Jost">
                        {title}
                    </h1>
                    <p className="news-content-text color-gray font-18 font-Jost">
                        {text}
                    </p>
                </div>

                <div className="news-card">
                    {
                        card.map((e, i) => {
                            return (
                                <Cards
                                    key={i}
                                    {...e}
                                />
                            )
                        }
                        )
                    }
                </div>
            </div>
        </>
    )
}
export default Index