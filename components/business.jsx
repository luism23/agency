import SourceImg from "@components/sourceimg"
import Link from "next/link"

const Cards = ({ title = "", text = "", textLink = "", link = "", img }) => {
    return (
        <>
            <div className="business-info">
                <img className="business-info-img" src={"/img/svg/" + img} alt={title} />
                <div className="business-info-content">
                    <h3 className="business-info-content-title color-gray-lite font-24 font-w-600 font-Jost">
                        {title}
                    </h3>
                    <p className="business-info-content-text color-gray font-18 font-Jost">
                        {text}
                    </p>
                    <Link href={link}>
                        <a className="business-info-content-textLink color-purple-2 font-18 font-w-600 font-Jost">
                            {textLink}
                            <img className="business-info-content-arrow" src={"/img/svg/flecha-link.svg"} alt="" />
                        </a>
                    </Link>
                </div>
            </div>
        </>

    )
}


const Index = ({ title, text, card = [] }) => {
    return (
        <>
            <div className="business">
                <div className="business-content">
                    <h1 className="business-content-title color-gray-lite font-48 font-w-700 font-Jost">
                        {title}
                    </h1>
                    <p className="business-content-text color-gray font-18 font-Jost">
                        {text}
                    </p>
                </div>

                <div className="business-card">
                    {
                        card.map((e, i) => {
                            return (
                                <Cards
                                    key={i}
                                    {...e}
                                />
                            )
                        }
                        )
                    }
                </div>
            </div>
        </>
    )
}
export default Index