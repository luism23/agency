import Link from "next/link"

import data from "@data/footer"

const LinkFooter = ({ link, text }) => {
    return (
        <>
            <Link href={link}>
                <a className="footer-linkInfo-text font-18 font-Jost color-white">{text}</a>
            </Link>
        </>
    )
}
const LinkFooterRS = ({ link, img }) => {
    return (
        <>
            <Link href={link}>
                <a className=" font-18 font-Jost color-white">
                    <img className="footer-contentInfo-img-rs" src={"/img/svg/" + img} alt="" />
                </a>
            </Link>
        </>
    )
}
const ColFooter = ({ title = "", links = [] }) => {
    return (
        <>
            <div className="footer-linkInfo">
                <h3 className="footer-linkInfo-title font-24 font-w-600 font-Jost color-white">
                    {title}
                </h3>
                {
                    links.map((e, i) => {
                        return (
                            <LinkFooter
                                key={i}
                                {...e}
                            />
                        )
                    })
                }
            </div>

        </>
    )
}

const Index = () => {
    const { title, text, cols, rs, textBottom } = data
    return (
        <>
            <div className="bg-purple-2">
                <footer className="footer  container">
                    <div className="footer-contentInfo">
                        <h1 className="footer-contentInfo-title font-36 font-Jost font-w-700 color-white">
                            {title}
                        </h1>
                        <p className="footer-contentInfo-text font-18 font-Jost color-white">
                            {text}
                        </p>
                        <div className="footer-contentInfo-img">
                            {
                                rs.map((e, i) => {
                                    return (
                                        <LinkFooterRS
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                    </div>
                    {
                        cols.map((e, i) => {
                            return (
                                <ColFooter
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }

                </footer>
                <div>
                    <p className="footer-bottom color-white font-Jost font-18">
                        {textBottom}
                    </p>
                </div>
            </div>

        </>
    )
}
export default Index
