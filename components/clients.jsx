import SourceImg from "@components/sourceimg"


const Clients = ({ title, text, img }) => {
    return (
        <>
            <div className="clients-content-elements">
                <div className="clients-content-elements-conteninfo">
                    <img className="clients-content-elements-conteninfo-img" src={"/img/svg/" + img} alt={title} />
                    <div className="clients-content-elements-conteninfo-titleText">
                        <h4 className="clients-content-elements-conteninfo-titleText-title color-gray-lite font-24 font-Jost font-w-600">
                            {title}
                        </h4>
                        <p className="clients-content-elements-conteninfo-titleText-text color-gray font-18 font-Jost">
                            {text}
                        </p>
                    </div>
                </div>
            </div>
        </>

    )
}
const Index = ({ title, textTitle, clients, img, imgBarra }) => {
    return (
        <>
            <div className="clients container">
                <div className="clients-contentTitle">
                    <h1 className="clients-title font-48 font-Jost font-w-700  color-gray-lite">
                        {textTitle}
                    </h1>
                </div>
                <picture className="clients-barra">
                    <SourceImg
                        img={imgBarra}
                        name="barra.png"
                        className="clients-barra"
                    />
                </picture>
                <picture className="clients-img">
                    <SourceImg
                        img={img}
                        name="clients.png"
                        className="clients-img"
                    />
                </picture>

                <div className="clients-content">
                    <h1 className="clients-content-title font-48 font-Jost font-w-700 color-gray-lite">
                        {title}
                    </h1>
                    <div className="clients-content-elements">
                        {
                            clients.map((e, i) => {
                                return (
                                    <Clients
                                        key={i}
                                        {...e}
                                    />
                                )
                            }
                            )
                        }
                    </div>

                </div>
                <div className="clients-void"></div>
            </div>
        </>
    )
}
export default Index