const Index = ({img,className="",name=""}) => {
    return (
        <>
            <source srcSet={`/img/1920/${img}`} media="(min-width: 1400px)" />
            <source srcSet={`/img/1400/${img}`} media="(min-width: 992px)" />
            <source srcSet={`/img/1000/${img}`} media="(min-width: 767px)" />
            <source srcSet={`/img/600/${img}`} media="(min-width: 575px)" />
            <img src={`/img/600/${img}`} alt={name} className={className} />
        </>
    )
}
export default Index