import data from "@data/header"
import Link from "next/link"
import { useState, useEffect ,useRef} from "react"



const LinkNav = ({ title = "", link = "" }) => {
    return (
        <Link href={link}>
            <a className="header-content-nav-subtitle color-gray font-Jost font-24">{title}</a>
        </Link>
    )
}

const Index = () => {
    const [scrollMenu,setScrollMenu] = useState(false)
    const header = useRef(null)
    useEffect(()=>{
        window.addEventListener("scroll",()=>{
            const scroll = window.scrollY
            const height = header.current.offsetHeight + 20
            setScrollMenu(scroll > height)
        })
    },[])
    return (
        <>
            <header ref={header} className={`header ${scrollMenu ? "bg-white" : ""}`}>
                <div className="container header-content" >
                    <div className="header-content-info">
                        <Link href={data.linkp.link}>
                            <a className="header-content-info-title font-Jost font-36 font-w-700">{data.linkp.title}</a>
                        </Link>
                    </div>
                    <nav className="header-content-nav">
                        {
                            data.links.map((e, i) => {
                                return (
                                    <LinkNav
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </nav>
                    <Link href={data.buttom.link}>
                        <a className="header-content-link color-purple-2 bg-white-2 font-18 font-Jost bg-gray-lite-hover color-white-2-hover">{data.buttom.title}</a>
                    </Link>
                </div>

            </header>
        </>
    )
}
export default Index