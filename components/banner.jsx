import SourceImg from "@components/sourceimg"
import Link from "next/link"



const Index = ({ title, text, link1, link2, bg }) => {
    return (
        <>
            <div className="banner ">
                <picture className="banner-fondo">
                    <SourceImg
                        img={bg}
                        name="senor.png"
                        className="banner-fondo"
                    />
                </picture>
                <div className="container">

                    <div className="banner-content ">
                        <div className="banner-content-titleInfo">
                            <h1 className="banner-content-titleInfo-title font-64 font-Jost font-w-700 color-gray-lite">
                                {title}
                            </h1>
                            <p className="banner-content-titleInfo-info">
                                {text}
                            </p>
                        </div>
                        <div className="banner-content-links">
                            <Link href={link1.link}>
                                <a className="banner-content-links-link-1 bg-purple-2 color-white font-18 font-Jost ">
                                    {link1.text}
                                    <img className="banner-content-links-link-1-img" src="/img/svg/flecha.svg" alt="flecha" />
                                </a>

                            </Link>
                            <Link href={link2.link}>
                                <a className="banner-content-links-link-2 font-18 font-w-500 font-Jost color-gray">
                                    <img className="banner-content-links-img" src="/img/svg/play-buttom.svg" alt="play-buttom" />{link2.text}</a>
                            </Link>
                        </div>

                    </div>
                    <div className="banner-void"></div>

                </div>
            </div>
        </>
    )
}
export default Index