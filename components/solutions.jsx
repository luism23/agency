import SourceImg from "@components/sourceimg"

const InfoNumber = ({ number = "", text = "", color = "" }) => {
    return (
        <>
            <div className="solutions-contentInfo-info bg-pink-3">
                <span className={`font-48 font-Jost font-w-700 ${color}`}>{number}</span>
                <span className="color-gray font-Jost font-18">{text}</span>
            </div>
        </>

    )
}

const Index = ({ title, text, infoNumber = [], bg }) => {
    return (
        <>
            <div className="solutions">
                <picture className="solutions-img">
                    <SourceImg
                        img={bg}
                        name="solutions.png"
                        className="solutions-img"
                    />
                </picture>
                <div className="solutions-contentInfo">
                    <h2 className="solutions-contentInfo-title color-gray-lite font-Jost font-48 font-w-700">
                        {title}
                    </h2>
                    <p className="solutions-contentInfo-text color-gray font-Jost font-18">
                        {text}
                    </p>
                    <div className="solutions-contentInfo-infoNumber">
                        {
                            infoNumber.map((e, i) => {
                                return (
                                    <InfoNumber
                                        key={i}
                                        {...e}
                                    />
                                )
                            }
                            )
                        }
                    </div>
                </div>


                <div className="solutions-void" ></div>
            </div>

        </>
    )
}
export default Index