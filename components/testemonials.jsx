import Link from "next/link"

const Cards = ({ title = "", subTitle = "", text = "", img = "" }) => {
    return (
        <>
            <div className="testemonials-infos">
                <div className="testemonials-infos-content">
                    <div className="testemonials-infos-content-2">
                        <img className="testemonials-infos-content-2-img" src={"/img/" + img} alt="" />
                        <h3 className="testemonials-infos-content-2-title color-gray-lite font-24 font-Jost">
                            {title}
                        </h3>
                        <h5 className="testemonials-infos-content-2-subTitle color-gray font-18 font-Jost">
                            {subTitle}
                        </h5>
                    </div>
                    <p className="testemonials-infos-content-text color-gray font-18 font-Jost">
                        {text}
                    </p>
                </div>
            </div>
        </>

    )
}


const Index = ({ title, text, card = [] }) => {
    return (
        <>
            <div className="testemonials">
                <div className="testemonials-content">
                    <h6 className="testemonials-content-title color-purple-2 font-24 font-Jost">
                        {title}
                    </h6>
                    <p className="testemonials-content-text color-gray-lite font-48 font-w-700 font-Jost">
                        {text}
                    </p>
                </div>

                <div className="testemonials-cards">
                    {
                        card.map((e, i) => {
                            return (
                                <Cards
                                    key={i}
                                    {...e}
                                />
                            )
                        }
                        )
                    }
                </div>
            </div>
        </>
    )
}
export default Index