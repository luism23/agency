const Index = ({ title, subTitle, text }) => {
    return (
        <>
            <div className="suscription container">
                <div className="void-2"></div>
                <div className="suscription-info">
                    <h4 className="suscription-info-subTitle suscription-title font-24 color-purple-2 font-Jost">
                        {subTitle}
                    </h4>
                    <h2 className="suscription-info-title font-48 font-w-700 color-gray-lite font-Jost">
                        {title}
                    </h2>
                    <p className="suscription-info-text font-18 font-Jost color-gray">
                        {text}
                    </p>
                    <div className="suscription-content border-color-gray">
                        <input className="suscription-content-input-email font-Jost font-24 color-gray " type="Email" placeholder="Enter your email..." />
                        <input className="suscription-content-input-submit font-Jost font-24 color-white bg-purple-2 bg-gray-lite-hover" type="submit" placeholder="Get Started" />
                    </div>
                </div>
                <div className="void-2"></div>
            </div>
        </>
    )
}
export default Index